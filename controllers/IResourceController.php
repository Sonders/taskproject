<?php
	interface IResourceController {
		public function index($params);

		public function create($params);

		public function get($params);

		public function edit($params);

		public function delete($params);
	}
?>