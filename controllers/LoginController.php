<?php

class LoginController
{
    public function index($params, $method)
    {
        if ($method == 'POST') {
            $login = isset($params['login']) && !empty($params['login']) ? $params['login'] : '';
            $password = isset($params['password']) && !empty($params['password']) ? $params['password'] : '';

            $this->login($login, $password);
        } else {
            include('views/login.php');
        }
    }

    private function login($login, $password)
    {
       $user = \Models\User::check($login, md5($password));

       if (isset($user) && !empty($user)) {
            session_start();

            $_SESSION['auth'] = 'true';

           header('Location: /');
       }

        header('Location: /');
    }
}