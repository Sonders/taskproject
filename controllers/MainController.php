<?php
    use Models\Task;

	class MainController implements IResourceController
	{
        public function index($params) {
            session_start();

            $page = 0;
            $sort = 'id';

            if (isset($params['page']) && !empty($params['page'])) {
                $page = $params['page'];
            }

            if (isset($params['sort']) && !empty($params['sort'])) {
                $sort = $params['sort'];
            }

            $tasks = Task::getAll($page, 3, $sort);
            $pageNumber = Task::getNumber() / 3;

            $isAuthorized = $_SESSION['auth'] == 'true' ? true : false;

            include 'views/main.php';
        }

        public function create($params)
        {
            Task::create($params['username'], $params['email'], $params['content'], 1);

            header('Location: /');
        }

        public function get($params)
        {

        }

        public function edit($params)
        {
            $status = $params['status'] == 'on' ? 2 : 1;

            Task::edit($params['id'], $params['content'], $status);

            header('Location: /');
        }

        public function delete($params)
        {

            header('Location: /');
        }
	}
?>