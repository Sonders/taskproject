<html>
	<head>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/styles.css"/>
    </head>

	<body>
        <div class="container">
            <div class="login-form">
                <div class="form-name">Авторизация</div><br />
                <form action="/login" method="POST">
                    <div class="form-group">
                        <input type="text" name="login" placeholder="Логин" class="form-control" required/><br />
                        <input type="password" name="password" placeholder="Пароль" class="form-control" required/><br />
                        <input type="submit" value="Войти" class="btn btn-success" class="form-control"/>
                    </div>
                </form>
            </div>
        </div>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>