<html>
	<head>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/styles.css"/>
    </head>

	<body>
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="tasks">
                            <h4 class="">Список задач</h4>

                            <div class="sort">
                                <span class="sort-label">Сортировать по: </span>
                                <?php if (isset($_GET['page']) && !empty($_GET['page'])): ?>
                                    <a href="?page=<?=$_GET['page']?>&sort=username">имени</a>
                                    <a href="?page=<?=$_GET['page']?>&sort=email">email</a>
                                    <a href="?page=<?=$_GET['page']?>&sort=status">статусу</a>
                                <?php else: ?>
                                    <a href="?sort=username">имени</a>
                                    <a href="?sort=email">email</a>
                                    <a href="?sort=status">статусу</a>
                                <?php endif; ?>
                            </div>

                            <ul class="list-group">
                                <li class="list-group-item">
                                    <?php foreach ($tasks as $task): ?>
                                        <form action="/edit" method="POST">
                                            <p>Имя: <strong><?=$task->username?></strong></p>
                                            <p>Email: <strong><?=$task->email?></strong></p>

                                            <?php if ($isAuthorized): ?>
                                                <div class="form-group">
                                                    <textarea name="content" class="form-control"><?=$task->content?></textarea><br />

                                                    <label for="status">Статус:</label>
                                                    <input type="checkbox" name="status" <?=$task->status == 2 ? 'checked' : '' ?> id="status"/><br /><br />

                                                    <input type="hidden" name="id" value="<?=$task->id?>"/>

                                                    <input type="submit" value="Сохранить" class="btn btn-dark" class="form-control"/><hr />
                                                </div>
                                            <?php else: ?>
                                                <p><?=$task->content?></p>
                                                <p>Статус: <?=$task->status == 2 ? 'Выполнено' : 'Не выполнено'?></p><hr />
                                            <?php endif; ?>
                                        </form>
                                    <?php endforeach; ?>
                                </li>
                            </ul>
                        </div>

                        <ul class="pagination">
                            <?php for ($i = 0; $i <= $pageNumber; $i++): ?>
                                <?php if (isset($_GET['sort']) && !empty($_GET['sort'])): ?>
                                    <li class="page-item"><a href="?sort=<?=$_GET['sort']?>&page=<?=$i?>" class="page-link"><?=$i + 1?></a></li>
                                <?php else: ?>
                                    <li class="page-item"><a href="?page=<?=$i?>" class="page-link"><?=$i + 1?></a></li>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </ul>
                    </div>

                    <div class="col-md-4">
                        <h4 class="">Добавить задачу</h4>

                        <form action="/create" method="POST" class="create-task">
                            <div class="form-group">
                                <label for="username">Ваше имя:</label>
                                <input type="text" name="username" class="form-control" id="username" required/>
                            </div>

                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" name="email" class="form-control" id="email" required/>
                            </div>

                            <div class="form-group">
                                <label for="content">Текст задачи:</label>
                                <textarea name="content" class="form-control" id="content" required></textarea>
                            </div>

                            <input type="submit" class="btn btn-primary" value="Сохранить"/>
                        </form>
                    </div>
                </div>
        </div>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>