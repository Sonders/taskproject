<?php
	namespace Models;

    use \RedBeanPHP\R;

	class Task {
        private static $tableName = 'tasks';

        public static function get(int $id)
        {
            $task = R::load(self::$tableName, $id);

            return $task;
        }

        public static function getAll(int $page, int $perPage, $sort = 'id') : array
        {
            $tasks = R::find(self::$tableName, 'ORDER BY '.$sort.' DESC LIMIT '.$page.', '.$perPage);

            return $tasks;
        }

        public static function getByField(string $field, string $value)
        {
            $tasks = R::find(self::$tableName, $field.' = '.$value);

            return $tasks;
        }

        public static function getNumber() : int
        {
            $tasksNumber = R::count('tasks');

            return $tasksNumber;
        }

        public static function create(string $username, string $email, string $content, int $status) : int
        {
            $task = R::dispense(self::$tableName);

            if (isset($username) && !empty($username)) {
                $task->username = $username;
            }

            if (isset($email) && !empty($email)) {
                $task->email = $email;
            }

            if (isset($content) && !empty($content)) {
                $task->content = $content;
            }

            if (isset($status) && !empty($status)) {
                $task->status = $status;
            }

            $id = R::store($task);

            return $id;
        }

        public static function edit(int $id, string $content, int $status)
        {
            $task = R::load(self::$tableName, $id);

            if (isset($content) && !empty($content)) {
                $task->content = $content;
            }

            if (isset($status) && !empty($status)) {
                $task->status = $status;
            }

            R::store($task);
        }

        public static function delete(int $id)
        {
            R::trash(self::$tableName, $id);
        }
	}
?>