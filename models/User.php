<?php
	namespace Models;

    use \RedBeanPHP\R;

	class User {
        private static $tableName = 'users';

        public static function get(int $id)
        {
            $user = R::load(self::$tableName, $id);

            return $user;
        }

        public static function getAll() : array
        {
            $users = R::find(self::$tableName);

            return $users;
        }

        public static function check(string $login, string $password)
        {
            $user = R::findOne(self::$tableName, 'login = :login AND password = :password', [':login' => $login, ':password' => $password]);

            return $user;
        }

        public static function create(int $personId, string $login, string $password) : int
        {
            $user = R::dispense(self::$tableName);

            if (isset($personId) && !empty($personId)) {
                $user->person_id = $personId;
            }

            if (isset($login) && !empty($login)) {
                $user->login = $login;
            }

            if (isset($password) && !empty($password)) {
                $user->password = md5($password);
            }

            $id = R::store($user);

            return $id;
        }

        public static function delete(int $id)
        {
            R::trash(self::$tableName, $id);
        }
	}
?>