<?php
	namespace Models;

    use \RedBeanPHP\R;

	class Person
    {
        private static $tableName = 'persons';

        public static function get(int $id)
        {
            $person = R::load(self::$tableName, $id);

            return $person;
        }

        public static function getAll() : array
        {
            $persons = R::find(self::$tableName);

            return $persons;
        }

        public static function getByField(string $field, string $value)
        {
            $persons = R::find(self::$tableName, ':field = :value', [':field' => $field, ':value' => $value]);

            return $persons;
        }

        public static function create(string $firstName, string $lastName, string $email) : int
        {
            $person = R::dispense(self::$tableName);

            if (isset($firstName) && !empty($firstName)) {
                $person->first_name = $firstName;
            }

            if (isset($lastName) && !empty($lastName)) {
                $person->last_name = $lastName;
            }

            if (isset($email) && !empty($email)) {
                $person->email = $email;
            }

            $id = R::store($person);

            return $id;
        }

        public static function edit(int $id, string $firstName, string $lastName, string $email)
        {
            $person = R::load(self::$tableName, $id);

            if (isset($firstName) && !empty($firstName)) {
                $person->first_name = $firstName;
            }

            if (isset($lastName) && !empty($lastName)) {
                $person->last_name = $lastName;
            }

            if (isset($email) && !empty($email)) {
                $person->email = $email;
            }

            R::store($person);
        }

        public static function delete(int $id)
        {
            R::trash(self::$tableName, $id);
        }
	}
?>