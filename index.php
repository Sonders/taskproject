<?php
	require_once 'vendor/autoload.php';

	require_once 'Routes.php';

    require_once 'models/Person.php';
    require_once 'models/User.php';
    require_once 'models/Task.php';

    require_once 'controllers/IResourceController.php';
	require_once 'controllers/MainController.php';
	require_once 'controllers/LoginController.php';

	use \RedBeanPHP\R;

	R::setup('mysql:host=localhost;dbname=taskproject;', 'user', 'admin11admin');

	$router = new Lemmon\Router\Router;

    $router->match('', function() {
        global $routes;

        if (array_key_exists('/', $routes)) {
            $controller = new $routes['/'];

            $controller->index($_GET);
        } else {
            header('HTTP/1.0 404 Not Found');
        }
    });

	$router->match('{path}', function($request) {
		global $routes;

		$params = [];

		switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                $params = $_POST;

                break;
            case 'GET':
                $params = $_GET;

                break;
            case 'PUT':

                break;
            case 'DELETE':

                break;
        }

        if ($request->path == 'edit' || $request->path == 'get' || $request->path == 'delete' || $request->path == 'create') {
            if (array_key_exists('/', $routes)) {
                $controller = new $routes['/'];

                $controller->{$request->path}($params, $_SERVER['REQUEST_METHOD']);
            } else {
                header('HTTP/1.0 404 Not Found');
            }
        } else {
            if (array_key_exists($request->path, $routes)) {
                $controller = new $routes[$request->path];

                $controller->index($params, $_SERVER['REQUEST_METHOD']);
            } else {
                header('HTTP/1.0 404 Not Found');
            }
        }
	});

	$router->dispatch();
?>